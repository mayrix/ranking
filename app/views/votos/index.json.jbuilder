json.array!(@votos) do |voto|
  json.extract! voto, :id, :empresa_id, :email, :calificacion_id, :area_id
  json.url voto_url(voto, format: :json)
end
