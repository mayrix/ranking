json.array!(@calificacions) do |calificacion|
  json.extract! calificacion, :id, :nombre, :numero
  json.url calificacion_url(calificacion, format: :json)
end
