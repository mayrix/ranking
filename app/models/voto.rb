class Voto < ActiveRecord::Base
  belongs_to :empresa
  belongs_to :area
  belongs_to :calificacion
  validates_presence_of :empresa_id
  validates_presence_of :email
  validates_presence_of :calificacion_id
  validates_presence_of :area_id
  validates :email, presence: true,  length: { minimum: 5 }, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, on: :create }
  #confirmation: true
  #validates :email_confirmation, presence: true
end
