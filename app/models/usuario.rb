class Usuario < ActiveRecord::Base
  validates_presence_of :email
  validates_presence_of :visible
  validates :email, uniqueness: true
end
