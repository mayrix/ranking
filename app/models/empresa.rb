class Empresa < ActiveRecord::Base
  has_many :votos, dependent: :destroy
  validates_presence_of :nombre
end
