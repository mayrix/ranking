class Calificacion < ActiveRecord::Base
  has_many :votos, dependent: :destroy
  validates_presence_of :nombre
  validates_presence_of :numero
end
