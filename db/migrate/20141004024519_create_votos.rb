class CreateVotos < ActiveRecord::Migration
  def change
    create_table :votos do |t|
      t.integer :empresa_id
      t.string :email
      t.integer :calificacion_id
      t.integer :area_id

      t.timestamps
    end
  end
end
