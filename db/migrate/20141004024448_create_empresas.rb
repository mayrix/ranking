class CreateEmpresas < ActiveRecord::Migration
  def change
    create_table :empresas do |t|
      t.string :nombre
      t.string :imagen
      t.boolean :visible

      t.timestamps
    end
  end
end
