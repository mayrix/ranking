class CreateCalificacions < ActiveRecord::Migration
  def change
    create_table :calificacions do |t|
      t.string :nombre
      t.integer :numero

      t.timestamps
    end
  end
end
